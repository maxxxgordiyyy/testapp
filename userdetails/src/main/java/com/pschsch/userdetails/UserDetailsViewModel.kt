package com.pschsch.userdetails

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pschsch.core.database.User
import com.pschsch.core.database.UsersRepositoryImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserDetailsViewModel : ViewModel() {
    val detailsLiveData = MutableLiveData<User>()

    private val repo = UsersRepositoryImpl()

    fun init(selectedId : String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                val user = repo.fetchUserById(selectedId)
                detailsLiveData.postValue(user)
            }
        }
    }
}