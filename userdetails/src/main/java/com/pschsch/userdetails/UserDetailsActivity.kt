package com.pschsch.userdetails

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.pschsch.core.web_service.ServiceFactory
import kotlinx.android.synthetic.main.activity_user_details.*


class UserDetailsActivity : AppCompatActivity() {

    private lateinit var viewModel: UserDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ActivityCompat.postponeEnterTransition(this)
        setContentView(R.layout.activity_user_details)
        supportActionBar?.title = getString(R.string.contact)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewModel = ViewModelProviders.of(this)[UserDetailsViewModel::class.java]
        viewModel.init(intent.getStringExtra(USER_ID))
        viewModel.detailsLiveData.observe(this, Observer {
            loadAndOptimizeAvatarTransition(it.userName)
            val nickNameVisibility = if (it.userName.isNullOrEmpty()) GONE else VISIBLE
            user_details_nickname.visibility = nickNameVisibility
            user_details_nickname_title.visibility = nickNameVisibility
            user_details_nickname.text = it.userName
            user_details_name.text = it.name
        })
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        if (menuItem.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(menuItem)
    }

    private fun loadAndOptimizeAvatarTransition(userName: String?) {
        if (userName.isNullOrEmpty()) {
            ActivityCompat.startPostponedEnterTransition(this)
            user_details_avatar.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.placeholder
                )
            )
            return
        }
        val requestOptions = RequestOptions.placeholderOf(R.drawable.placeholder)
            .dontTransform()
            .onlyRetrieveFromCache(true)
        Glide.with(this)
            .load(ServiceFactory.usersAvatarUrl(userName.orEmpty()))
            .apply(requestOptions)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    ActivityCompat.startPostponedEnterTransition(this@UserDetailsActivity)
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    ActivityCompat.startPostponedEnterTransition(this@UserDetailsActivity)
                    return false
                }
            })
            .into(user_details_avatar)
    }

    companion object {
        const val USER_ID = "usID"
    }
}
