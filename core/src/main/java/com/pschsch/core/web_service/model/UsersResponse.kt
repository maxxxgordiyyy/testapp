package com.pschsch.core.web_service.model

import com.google.gson.annotations.SerializedName
import com.pschsch.core.database.User


data class UsersResponse(

    @SerializedName("members") val members: List<User>,
    @SerializedName("count") val count: Int,
    @SerializedName("offset") val offset: Int,
    @SerializedName("total") val total: Int,
    @SerializedName("success") val success: Boolean
)