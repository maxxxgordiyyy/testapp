package com.pschsch.core.web_service

import com.pschsch.core.web_service.model.UsersResponse
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface UsersClient {
    @Headers(
        value = [
            "X-User-Id: e2qQQA3adTsZGN8et",
            "X-Auth-Token: kc8AHBZlH3iCObf9alUrfpDkUoBAEsYoEEw3JBemQq3"
        ]
    )
    @GET("api/v1/channels.members?roomName=general")
    suspend fun retrieveUsers(
        @Query("count") count: Int,
        @Query("offset") offset: Int
    ): UsersResponse
}