package com.pschsch.core.web_service

import com.pschsch.core.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import retrofit2.create
import java.util.concurrent.TimeUnit


object ServiceFactory {

    fun usersAvatarUrl(userName: String) =
        String.format("https://open.rocket.chat/avatar/%s?format=jpeg", userName)

    private inline fun <reified Type> createService(): Type {

        val builder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://open.rocket.chat/")

        val okHttp = OkHttpClient.Builder().apply {
            if (BuildConfig.DEBUG) {
                addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
            }
            readTimeout(60, TimeUnit.SECONDS)
            connectTimeout(60, TimeUnit.SECONDS)
            writeTimeout(60, TimeUnit.SECONDS)
        }
        return builder.client(okHttp.build()).build().create(Type::class.java)
    }

    fun usersService(): UsersClient {
        return createService()
    }


}