package com.pschsch.core.database

import androidx.paging.DataSource
import com.pschsch.core.CoreApplication
import kotlinx.coroutines.*
import java.lang.Exception

class UsersRepositoryImpl {

    private val scope = CoroutineScope(Dispatchers.Main)
    private var saveJob: Job? = null

    private val usersDatabase: UsersDatabase = UsersDatabase.getDatabase(CoreApplication.app)
    private val userDao = usersDatabase.usersDao()

    fun clearUsers() {
        userDao.truncateUsers()
    }

    fun fetchUsers() : DataSource.Factory<Int, User> {
        return userDao.fetchUsers()
    }

    fun fetchUserById(id : String) : User {
        return userDao.fetchUserById(id)
    }

    fun saveUsers(users: List<User>) {
        if(saveJob != null) return
        saveJob = scope.launch {
            try {
                withContext(Dispatchers.IO) {
                    userDao.saveUsers(users)
                }
            } catch (e : Exception) {
                e.printStackTrace()
            }
        }

    }
}