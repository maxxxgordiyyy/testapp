package com.pschsch.core.database

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = [User::class], version = 2, exportSchema = false)
abstract class UsersDatabase : RoomDatabase() {

    abstract fun usersDao(): UsersDao

    companion object {
        @Volatile
        private var usersDbInstance: UsersDatabase? = null
        private val lock = Any()

        fun getDatabase(context: Context) = usersDbInstance ?: synchronized(lock) {
            Log.d("UsersDatabase", "db")
            usersDbInstance ?: buildDatabase(context).also { usersDbInstance = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context,
            UsersDatabase::class.java, "users.db"
        ).fallbackToDestructiveMigration().build()
    }

}