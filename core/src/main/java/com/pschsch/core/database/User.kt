package com.pschsch.core.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "user_table")
class User {
    @PrimaryKey
    @ColumnInfo(name = "_id")
    @SerializedName("_id")
    var userId: String = ""
    @ColumnInfo(name = "status")
    @SerializedName("status")
    var status: String = ""
    @ColumnInfo(name = "name")
    @SerializedName("name")
    var name: String = ""
    @ColumnInfo(name = "utcOffset")
    @SerializedName("utcOffset")
    var utfOffset: Double? = null
    @ColumnInfo(name = "username")
    @SerializedName("username")
    var userName: String? = null



    companion object {
        const val STATUS_OFFLINE = "offline"
        const val STATUS_ONLINE = "online"
    }

    override fun toString(): String {
        return "User(userId='$userId', status='$status', name='$name', utfOffset=$utfOffset, userName=$userName)"
    }
}