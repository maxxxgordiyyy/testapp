package com.pschsch.core.database

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface UsersDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveUsers(users: List<User>)

    @Query("SELECT * FROM user_table")
    fun fetchUsers(): DataSource.Factory<Int, User>

    @Query("DELETE FROM user_table")
    fun truncateUsers()

    @Query("SELECT COUNT(*) FROM user_table")
    fun usersSize(): Int

    @Query("SELECT * FROM user_table WHERE _id = :id")
    fun fetchUserById(id : String) : User


}