package com.pschsch.core

import android.app.Application

open class CoreApplication : Application(){
    override fun onCreate() {
        super.onCreate()
        app = this
    }

    companion object {
        lateinit var app: CoreApplication
            private set
    }
}