package com.pschsch.userfeed

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.pschsch.core.database.User
import com.pschsch.core.web_service.ServiceFactory
import kotlinx.android.synthetic.main.rv_item_user_feed.view.*

class UserFeedAdapter(callback: DiffUtil.ItemCallback<User>, private val userClick: (Int) -> Unit) :
    PagedListAdapter<User, UserFeedAdapter.UserFeedViewHolder>(callback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserFeedViewHolder {
        return UserFeedViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.rv_item_user_feed,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: UserFeedViewHolder, position: Int) = holder.bind(position)

    private fun UserFeedViewHolder.bind(position: Int) {
        val userInfo = getItem(position)
        val usName = userInfo?.userName
        if (!usName.isNullOrEmpty() && usName != "?" && usName != "&") {
            Glide.with(avatar.context).load(ServiceFactory.usersAvatarUrl(usName)).into(avatar)
        } else {
            avatar.setImageDrawable(
                ContextCompat.getDrawable(
                    avatar.context,
                    R.drawable.placeholder
                )
            )
        }
        status.setImageDrawable(
            if (userInfo?.status == User.STATUS_OFFLINE) {
                ContextCompat.getDrawable(status.context, R.drawable.ic_offline)
            } else {
                ContextCompat.getDrawable(status.context, R.drawable.ic_online)
            }
        )
        name.text = if (userInfo?.name == null) userInfo?.userName else userInfo.name
    }


    inner class UserFeedViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val avatar = view.item_user_feed_avatar
        val status = view.item_user_feed_status
        val name = view.item_user_feed_name

        init {
            view.setOnClickListener {
                userClick.invoke(adapterPosition)
            }
        }
    }
}