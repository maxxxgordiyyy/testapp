package com.pschsch.userfeed

import android.util.Log
import androidx.lifecycle.*
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.pschsch.core.database.User
import com.pschsch.core.database.UsersRepositoryImpl
import com.pschsch.core.web_service.ServiceFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.Executors


class UserFeedViewModel : ViewModel() {

    var initialLiveData: LiveData<PagedList<User>> = MutableLiveData<PagedList<User>>()
    val acceptChoiceLiveData = MutableLiveData<Pair<Int, User>>()

    private val repo = UsersRepositoryImpl()
    private val client = ServiceFactory.usersService()

    private var initialPagedList: PagedList<User>? = null

    private var loadEndJob: Job = Job()

    private val pagedListObserver = Observer<PagedList<User>> {
        initialPagedList = it
    }


    init {
        initUsers()
    }

    private fun initUsers() {
        val config = PagedList.Config.Builder()
            .setPageSize(PAGE_SIZE)
            .setPrefetchDistance(2)
            .setInitialLoadSizeHint(10)
            .setEnablePlaceholders(true)
            .build()
        initialLiveData = LivePagedListBuilder(
            repo.fetchUsers(),
            config
        ).setFetchExecutor(Executors.newSingleThreadExecutor())
            .setBoundaryCallback(object : PagedList.BoundaryCallback<User>() {
                override fun onZeroItemsLoaded() {
                    Log.e("VM", "zero")
                    viewModelScope.launch {
                        val initialUsers = fetchUsersFromNetwork(PAGE_SIZE, 0)
                        repo.saveUsers(initialUsers)
                    }
                }

                override fun onItemAtEndLoaded(itemAtEnd: User) {
                    if (loadEndJob.isActive) {
                        loadEndJob.cancel()
                    }
                    loadEndJob = viewModelScope.launch {
                        val users = fetchUsersFromNetwork(
                            PAGE_SIZE,
                            initialPagedList.orEmpty().size + PAGE_SIZE
                        )
                        repo.saveUsers(users)
                    }
                }
            }).build()
        initialLiveData.observeForever(pagedListObserver)
    }

    fun onItemClick(position: Int) {
        initialPagedList?.get(position)?.let {
            acceptChoiceLiveData.value = position to it
        }

    }

    private suspend fun fetchUsersFromNetwork(count: Int, offset: Int): List<User> {
        return withContext(Dispatchers.IO) {
            client.retrieveUsers(count, offset)
        }.members
    }

    override fun onCleared() {
        initialLiveData.removeObserver(pagedListObserver)
        super.onCleared()
    }

    companion object {
        const val PAGE_SIZE = 10
    }
}