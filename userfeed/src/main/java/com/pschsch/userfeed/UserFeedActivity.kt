package com.pschsch.userfeed

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Pair
import android.view.ViewTreeObserver
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.pschsch.core.database.User
import com.pschsch.userdetails.UserDetailsActivity
import kotlinx.android.synthetic.main.activity_user_feed.*

class UserFeedActivity : AppCompatActivity() {

    private lateinit var userFeedViewModel: UserFeedViewModel

    private lateinit var usersAdapter: PagedListAdapter<User, *>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_feed)
        userFeedViewModel = ViewModelProviders.of(this)[UserFeedViewModel::class.java]
        user_feed_recycler_view.apply {
            usersAdapter = UserFeedAdapter(UserItemCallback) {
                userFeedViewModel.onItemClick(it)
            }
            adapter = usersAdapter
        }
        supportActionBar?.title = getString(R.string.contacts)
        userFeedViewModel.initialLiveData.observe(this, Observer {
            usersAdapter.submitList(it)
        })
        userFeedViewModel.acceptChoiceLiveData.observe(this, Observer { user ->
            user_feed_recycler_view.findViewHolderForLayoutPosition(user.first)?.itemView?.let {
                val view = it.findViewById<ImageView>(R.id.item_user_feed_avatar)
                val opt = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    this, view,
                    ViewCompat.getTransitionName(view).orEmpty()
                )
                startActivity(Intent(this, UserDetailsActivity::class.java).also { int ->
                    int.putExtra(UserDetailsActivity.USER_ID, user.second.userId)
                }, opt.toBundle())
            }

        })
    }


    private object UserItemCallback : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean =
            oldItem.userId == newItem.userId

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean = true
    }
}
